import React from 'react';
import ReactDOM from 'react-dom/client';
import './App.css';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Container } from 'react-bootstrap';
import { UserProvider } from './UserContext';
import { useState, useEffect, useContext } from 'react';


//    [COMPONENTS]
import AppNavBar from './components/AppNavBar.js'

//    [PAGES]
import Register  from './pages/Register.js';
import Login from './pages/Login';
import Logout from './pages/Logout';
import CreateProduct from './pages/CreateProduct';
import RetrieveAllProduct from './pages/RetrieveAllProduct';
import UpdateProduct from './pages/UpdateProduct';
import CreateOrder from './pages/CreateOrder';
import Product from './pages/Product';

import Home from './pages/Home';

function App() {

  const [userAdmin, setUserAdmin] = useState({
      id: null,
      isAdmin : null
  }) 



  return (
    <UserProvider value={{userAdmin, setUserAdmin}}>
    <>
      <Router>
        <AppNavBar/>
        <Container>
           <Routes>
            <Route exact path="/" element={<Home/>} />
              <Route exact path="/register" element={<Register/>} />
              <Route exact path="/login" element={<Login/>} />
              <Route exact path="/logout" element={<Logout/>} />
              <Route exact path="/createProduct" element={<CreateProduct/>} />
              <Route exact path="/retrieveAllProduct" element={<RetrieveAllProduct/>} />
              <Route exact path="/updateProduct" element={<UpdateProduct/>} />
              <Route exact path="/createOrder" element={<CreateOrder/>} />
              <Route exact path="/product" element={<Product/>} />
           </Routes>
        </Container>
      </Router>
    </>
    </UserProvider>
  );
}

export default App;
