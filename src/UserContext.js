import React from 'react';

//creates a Context Object
const UserContext = React.createContext();

//Creates a provider component
//allows other components to consume/use context
//object and supply the necessary info needed to the
//context object
export const UserProvider = UserContext.Provider;

export default UserContext;
    
