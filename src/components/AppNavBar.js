import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { NavLink, Link, useSearchParams } from 'react-router-dom';
import UserContext from '../UserContext';
import { useContext, useEffect, useState } from 'react';
import Highlight from '../components/Highlight'


export default function AppNavBar() {


  const [userIsAdmin, setUserIsAdmin] = useState(false);
  const [isThereUser, setIsThereUser] = useState(false);



  //check if there is a user
  //if so, hide register
  // if no, hide
  // useEffect(() => {
  //   if (localStorage.length > 0) {
  //     setIsThereUser(true)
  //   } else {
  //     setIsThereUser(false)
  //   } 
  // }, [localStorage.length])


  //if admin user
  //hide the nav bar
  // useEffect(() => {
  //   if (localStorage.getItem('isAdmin') === true) {
  //     setUserIsAdmin(true)

  //   } else {
  //     setUserIsAdmin(false)
  //   }
  // },[localStorage.getItem('isAdmin')])


  return (
    <Navbar bg="warning" expand="" className="p-3">
        <Navbar.Brand href="/ ">LaZhoppe</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />

        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto m-1 ">
                <Nav.Link as={NavLink} exact to="/">Home</Nav.Link>
                <Nav.Link as={NavLink} exact to="/product">Product</Nav.Link>  
                
   
                {/* IF THERE IS USER, HIDE REGISTER, IF NONE HIDE LOGOUT */}
             
                <Nav.Link as={NavLink} exact to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} exact to="/register">Register</Nav.Link>
                <Nav.Link as={NavLink} exact to="/logout">Logout</Nav.Link>
                
                <Nav.Link as={NavLink} exact to="/createProduct">Create Product</Nav.Link>
                <Nav.Link as={NavLink} exact to="/RetrieveAllProduct">Retrieve Product</Nav.Link>        
                <Nav.Link as={NavLink} exact to="/updateProduct">Update Product</Nav.Link>    
                <Nav.Link as={NavLink} exact to="/createOrder">Create Order</Nav.Link>  
          </Nav>
        </Navbar.Collapse>
    </Navbar>

    
  );
}

