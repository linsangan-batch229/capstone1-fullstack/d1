import { useEffect, useState } from 'react';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function CreateProduct() {

    const [productName, setProductName] = useState('');
    const [productDesc, setProductDesc] = useState('');
    const [productPrice, setProductPrice]= useState('');
    const [productQuantity, setProductQuantity]= useState('');
    const [createBtnActive, setCreateBtnActive ]= useState(false);


    //disabled button if the fields are not fullfilled
    useEffect((e) => {
        if (productName !== '' && productDesc !== '' && productPrice > 0 && productQuantity > 0) {
            setCreateBtnActive(true);
        } else {
            setCreateBtnActive(false);
        }   

    }, [productName, productDesc, productPrice, productQuantity])
    


    //      [MAIN FUNCTION]
    function createThisProduct(e) {

        const token = localStorage.getItem('token');

        fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
            method : 'POST',
            headers : {
                Authorization: `Bearer ${token}`,
                'Content-Type' : 'application/json; charset=UTF-8'
            },
            body : JSON.stringify({
                product_name : productName,
                product_description : productDesc,
                product_price : productPrice,
                product_quantity : productQuantity
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log("data" + data)
            console.log("data" + typeof data)
            
            //if data === true
            // meaning authorized or is an admin
            if (data === false) {

                Swal.fire({
                    title: 'Failed',
                    icon: 'error',
                    text : 'Not authorized'
                })


            //false and not an admin
            } else {

                //call function !'isProductExistAlready' from backed
                // use reverse '!'
                // recived as false. then use '!' to make it true
                // final data === true, meaning no duplicate
                // if === false, meaning has duplicate
                Swal.fire({
                    title : 'Success',
                    icon: "success",
                    text: "Successfully created the product."
                })

            }

        })
    }






    return(
        <>
        
        <Container  className='pt-4 pb-3 text-center' bg="warning">
            <h4>CREATE PRODUCT</h4>
        </Container>

        <Form onSubmit={(e) => createThisProduct(e)} >
        <Container>
        {/* PRODUCT NAME */}
        <Form.Group className="mb-3" controlId="formBasicProdName">
            <Form.Label>Product Name</Form.Label>
            <Form.Control 
            type="text" 
            placeholder="Name" 
            value={productName}
            onChange={(e) => setProductName(e.target.value)}
            required
            />
        </Form.Group>

         {/* PRODUCT DESCRIPTION */}
        <Form.Group className="mb-3" controlId="formBasicProdPass">
            <Form.Label>Product Description</Form.Label>
            <Form.Control 
            type="text" 
            placeholder="Description" 
            value={productDesc}
            onChange={(e) => setProductDesc(e.target.value)}
            required    
            />
        </Form.Group>

        {/* PRODUCT PRICE */}
        <Form.Group className="mb-3" controlId="formBasicProdPrice">
        <Form.Label>Product Price</Form.Label>
        <Form.Control 
            type="Number" 
            placeholder="0" 
            value={productPrice}
            onChange={(e) => setProductPrice(e.target.value)}
            required
            />
        </Form.Group>


        {/* PRODUCT QUANTITY */}
        <Form.Group className="mb-3" controlId="formBasicProdQuantity">
        <Form.Label>Product Quantity</Form.Label>
        <Form.Control 
            type="Number" 
            placeholder="0" 
            value={productQuantity}
            onChange={(e) => setProductQuantity(e.target.value)}
            required
            />
        </Form.Group>


        {
            (createBtnActive) ? 
            <Button variant="primary" type="submit">
                Create
            </Button>
            :
            <Button variant="primary" type="submit" disabled>
                Create
            </Button>
        }
   


        </Container>
        </Form>
        </>
    )
}