import { useEffect, useState } from "react"
import {  Button } from "react-bootstrap"
import { Container } from "react-bootstrap"
import { Form } from "react-bootstrap"
import Swal from "sweetalert2"



export default function CreateOrder(){

    const [searchById, setSearchById] = useState('');
    const [quantity, setQuantity] = useState(0);
    const [createOrderBtn, setCreateOrderBtn] = useState(false);

    //varaiable to calculate the grand total price 
    let allPrice = 0;
    
    //get the latest order
    let latestOrder = 'none';

    //disabled /enabled order button
    useEffect(() => {
        if (searchById !== '' && quantity > 0) {
            setCreateOrderBtn(true)
        } else {
            setCreateOrderBtn(false)
        }
    })


    //          [FUNCTION TO CREATE ORDER]

    function createOrder(e) {
        e.preventDefault();

        //need 3 parameter
        // userId, productId, quantity
        const userId = localStorage.getItem('id');
       // http://localhost:4000/orders/createOrder
       // https://capstone2-backend.onrender.com/orders/createOrder
  

        fetch(`${process.env.REACT_APP_API_URL}/orders/createOrder`, {
            method : 'POST',
            headers : {
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify({
                order_userId :  userId,
                order_productId :  searchById,
                order_productQuantity : quantity
            })
        })
        .then(res => res.json())
        .then(data => {

            //testing only
            // console.log("rawdata: " +  data.order_products.length);
            console.log("result: " + typeof data);

            //if data is false, meaning the user is an admin user
            if(data === false) {

                Swal.fire({
                    title : 'Failed',
                    icon : 'error',
                    text : 'Admin user cannot create order'
                })

            //error to catch if the order is higher than current stocks
            } else if (typeof data === 'number') {

                Swal.fire({
                    title : 'Failed',
                    icon : 'error',
                    text : `Avaiable stocks for this product is only : ${data}`
                })


            //data is true, meaning the user is not an admin
            } else {

                

                //calculate right away the total price before rendering it
                for (let i = 0; i < data.order_products.length; i++) {
                    console.log(allPrice);
                    allPrice += data.order_products[i].order_productTotalPrice;
                }

                //get the latest order id
                latestOrder = data.order_products[data.order_products.length - 1]._id;
                document.querySelector('#qwe').innerHTML = `Your latest Order ID : ${latestOrder}`;

 
                //function call to render the data
                renderData(data);
   
                

                Swal.fire({
                    title: 'Success',
                    icon : 'success',
                    text : 'Successfully place an order!'
                })


            }
        })       
    } //end of createOrder()



    //          [FUNCTION TO RENDER RECIEVED DATA FROM SERVER]
    function renderData(data) {
    
        
        //every the button has been clicked, it will reset the value to ''
        document.querySelector('#renderResult').value  = '';

        // // Show the total number of ordered items of a user 
        let totalOrderedItems = `Total Ordered Items : ${data.order_products.length}`
        let grandTotalPrice =  `Total Price of all items : ${allPrice}`
        
       

        document.querySelector('#renderResult').value  = `${totalOrderedItems}\n`;    
        document.querySelector('#renderResult').value  += `${grandTotalPrice}\n\n`;   

        for (let i = 0; i < data.order_products.length; i++) {

            let a = `Order ID : ${data.order_products[i]._id}`
            let b = `Product ID : ${data.order_products[i].order_productId}`
            let c = `Name : ${data.order_products[i].order_productName}`
            let d = `Description : ${data.order_products[i].order_productDescription}`
            let e = `Price : ${data.order_products[i].order_productPrice} `
            let f = `Quantity : ${data.order_products[i].order_productQuantity}`
            let g = `Total Price : ${data.order_products[i].order_productTotalPrice}`
            let h = `Created : ${data.order_products[i].order_createdOn}`
            

            document.querySelector('#renderResult').value += `\n${a}`;
            document.querySelector('#renderResult').value += `\n${b}`;
            document.querySelector('#renderResult').value += `\n${c}`;
            document.querySelector('#renderResult').value += `\n${d}`;
            document.querySelector('#renderResult').value += `\n${e}`;
            document.querySelector('#renderResult').value += `\n${f}`;
            document.querySelector('#renderResult').value += `\n${g}`;
            document.querySelector('#renderResult').value += `\n${h}\n\n`;
        }
    }


    return (
    <>
        <Container  className='pt-4 pb-3 text-center' bg="warning">
            <h3>CREATE ORDER</h3>
        </Container> 

        <Container>

            <Form onSubmit={(e) => createOrder(e)}>

            {/* PRODUCT ID */}
            <Form.Group className="mb-4" controlId="exampleForm.ControlInput1" >
                <Form.Label>Game ID</Form.Label>
                <Form.Control 
                    type="string" 
                    placeholder="Game ID" 
                    value = {searchById}
                    onChange = {(e) => setSearchById(e.target.value)}       
                    required
                /> 
            </Form.Group>

            {/* QUANTITY FIELD */}
            <Form.Group className="mb-2" controlId="exampleForm.ControlInput1" >
                <Form.Label>Quantiy</Form.Label>
                <Form.Control 
                    type="Number" 
                    placeholder="Quantity"     
                    value = {quantity}
                    onChange = {(e) => setQuantity(e.target.value)}   
                    required
                /> 
            </Form.Group>
            

            {/* TEXT AREA */}
            {/* CONDITIONAL RENDERING DISABLED/ENABLE BUTTON */}
            {   
                (createOrderBtn) ?
                <Button variant="success" type="submit" className="mb-2 mt-3">
                    Order
                </Button>
                :
                <Button variant="success" type="submit" className="mb-2 mt-3" disabled>
                    Order
                </Button>
            }


            {/* TEXT AREA */}
            <Form.Group className="mt-3" controlId="exampleForm.ControlTextarea1">
                <Form.Label id="qwe"></Form.Label>
                <Form.Control 
                    as="textarea" 
                    rows={10}  
                    value= ''
                    id = "renderResult"
                    placeholder=""
                    disabled  
                />
            </Form.Group>

            </Form>
        </Container>
    </>
    )

}