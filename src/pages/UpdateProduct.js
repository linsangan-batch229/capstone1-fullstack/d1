import { useEffect, useState } from 'react';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';



export default function UpdateProduct() {

    const [searchById, setSearchById] = useState('');
    const [update_productInfo, setUpdate_productInfo] = useState('');

    const [update_productName, setUpdate_productName] = useState('');
    const [update_productDesc, setUpdate_productDesc] = useState('');
    const [update_productPrice, setUpdate_productPrice] = useState('');
    const [update_productQuantity, setUpdate_productQuantity] = useState('');
    const [update_productIsActive, setUpdate_productIsActive] = useState('');



    const [findBtn, setFindBtn]= useState(false);
    const [update_isBtnActive, setUpdate_isBtnActive ]= useState(false);

    //button find product Id
    useEffect((e) => {

        if(searchById !== '') {
            setFindBtn(true)
        } else {
            setFindBtn(false)
        }

    }, [searchById])

    //button update 
    useEffect((e) => {

        if (update_productName !== '' && update_productDesc !== '' 
        && update_productPrice !== '' && update_productQuantity !== ''
        && update_productIsActive !== '') {

            setUpdate_isBtnActive(true);
        } else {
            setUpdate_isBtnActive(false);
        }

    })



    //          [FUNCTION FIND ID]
    function findOne(e) {
        
        //stop auto refresh
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/getProduct/${searchById}`, {
            headers : {
                'Content-Type' : 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {

            //test only
            console.log(data);

            //error or not found
            if(data === false) {

                Swal.fire({
                    title : 'Product Id Not Found',
                    icon: 'error',
                    text : ''
                })

            } else {

                const {product_name, product_description, product_price, product_quantity, product_isActive} = data

                setUpdate_productName(product_name);
                setUpdate_productDesc(product_description);
                setUpdate_productPrice(product_price);
                setUpdate_productQuantity(product_quantity);
                setUpdate_productIsActive(product_isActive);

                Swal.fire({
                    title : 'Success',
                    icon: 'success',
                    text : ''
                })


            }
        })
    }




    //          [FUNCTION TO UPDATE THE PRODUCT INFORMATION]
    function updateProductInfo(e) {
        //prevent from refreshing automatically
        e.preventDefault();

        //sample productID
        //63d0711f81732b402e0375c6 - Avatar
        //63d07bbe5b41132d816c7437 - Horizon Zero Dawn


        //need 3 parameters
        // Params, isAdmin from token, body
        const token = localStorage.getItem('token');
   
        fetch(`${process.env.REACT_APP_API_URL}/products/updateProductInfo/${searchById}`, {
            method : 'PUT',
            headers: {
                Authorization : `Bearer ${token}`,
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify({
                product_name : update_productName,
                product_description  :  update_productDesc,
                product_price : update_productPrice,
                product_quantity : update_productQuantity,
                product_isActive : update_productIsActive
        })  
        })
        .then(res => res.json())
        .then(data => {

            //for testing only
            console.log("update is :" + data);

            //data is false
            if(data === false) {
   
                Swal.fire({
                    title : 'Failed',
                    icon: 'error',
                    text : 'Not Authorized'
                })
            } else {

                Swal.fire({
                    title : 'Successfully updated',
                    icon: 'success',
                    text : ''
                })

                //reset fields
                setUpdate_productName('');
                setUpdate_productDesc('');
                setUpdate_productPrice('');
                setUpdate_productQuantity('');
                setUpdate_productIsActive('');
            }
        })
    }


 

    return(
        <>
        <Container  className='pt-4 pb-3 text-center' bg="warning">
            <h4>UPDATE PRODUCT INFORMATION</h4>
        </Container>    


        <Container>
        <Form onSubmit={(e) => findOne(e)}>
            {/* PRODUCT ID */}
            <Form.Group className="mb-4" controlId="exampleForm.ControlInput1">
                <Form.Label></Form.Label>
                <Form.Control 
                    type="string" 
                    placeholder="Product ID"        
                    value={searchById}
                    onChange={(e) => setSearchById(e.target.value)}
                    required
                />
            </Form.Group>

            {
                (findBtn) ? 
                <Button variant="primary" type="submit">
                    Find
                </Button>
                :
                <Button variant="primary" type="submit" disabled>
                    Find
                </Button>
            }
        </Form>

        <Form onSubmit={(e) => updateProductInfo(e)} className='pt-5' >  
            {/* PRODUCT NAME FIELD */}
            <Form.Group className="mb-4" controlId="exampleForm.ControlInput1">
                <Form.Label>Product Name</Form.Label>
                <Form.Control 
                    type="string" 
                    placeholder="Name"        
                    value={update_productName}
                    onChange = {(e) => setUpdate_productName(e.target.value)}
                    required
                />
            </Form.Group>

            {/* PRODUCT DESCRPTION FIELD */}
            <Form.Group className="mb-4" controlId="exampleForm.ControlInput1">
                <Form.Label>Product Description</Form.Label>
                <Form.Control 
                    type="string" 
                    placeholder="Description"        
                    value={update_productDesc}
                    onChange = {(e) => setUpdate_productDesc(e.target.value)}
                    required
                />
            </Form.Group>


            {/* PRODUCT PRICE FIELD */}
            <Form.Group className="mb-4" controlId="exampleForm.ControlInput1">
                <Form.Label>Product Price</Form.Label>
                <Form.Control 
                    type="string" 
                    placeholder="Price"        
                    value={update_productPrice}
                    onChange = {(e) => setUpdate_productPrice(e.target.value)}
                    required
                />
            </Form.Group>


            {/* PRODUCT QUANTITY FIELD */}
            <Form.Group className="mb-4" controlId="exampleForm.ControlInput1">
                <Form.Label>Product Quantity</Form.Label>
                <Form.Control 
                    type="string" 
                    placeholder="Quantity"        
                    value={update_productQuantity}
                    onChange = {(e) => setUpdate_productQuantity(e.target.value)}
                    required
                />
            </Form.Group>

            {/* PRODUCT ISADMIN FIELD */}
            <Form.Group className="mb-4" controlId="exampleForm.ControlInput1">
                <Form.Label>Product Activate/Deactivate</Form.Label>
                <Form.Control 
                    type="string" 
                    placeholder="true or false"        
                    value={update_productIsActive}
                    onChange = {(e) => setUpdate_productIsActive(e.target.value)}
                    required
                />
            </Form.Group>


            {
                (update_isBtnActive) ? 
                <Button variant="success" type="submit">
                    Update
                </Button>
                :
                <Button variant="success" type="submit" disabled>
                    Update
                </Button>
            }
        </Form>

        </Container>




    </>
    )
}