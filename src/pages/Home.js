import { Container } from 'react-bootstrap'
import Banner from '../components/Banner.js'
import { Card } from 'react-bootstrap'



const data = {
	title: "LaZhoppe Gamestore!",
	destination: "/product",
	label: "More Games"
}

export default function Home() {

    return (

    <>  
        <Banner data={data}/>
    
    <Container >

        <Card className="bg-dark text-white " style={{width: "100%"}}  >
            <Card.Img  src="https://images2.alphacoders.com/949/949069.jpg"  alt="Card image" style={{height : "59vh"}} />
        
            <Card.ImgOverlay>
                <Card.Title variant="success">SALE GAMES UP TO 40%</Card.Title>
                <Card.Text>
                Pre Order Bonus: Exclusive Summon Materia DLC and an Exclusive Reversible Poster
                </Card.Text>
                <Card.Text>Avaiable now!</Card.Text>
            </Card.ImgOverlay>
        </Card>

    </Container>
        
    </>


    )

}