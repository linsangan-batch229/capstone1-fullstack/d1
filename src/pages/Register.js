import {Form, Button} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';



export default function Register(){

    //info field
    // email
    // pasword
    // confirm password

    const navigate = useNavigate();


    //      [HOOKS]
    //state hooks -> store values of the input fields
    const [userEmail, setUserEmail] = useState("");
    const [userPassword1, setUserPassword1] = useState("");
    const [userPassword2, setUserPassword2] = useState("");
    //for register button
    const [btnIsActive, setBtnIsActive] = useState(false);  

    // validation to enable register button when all fields are
    //populated and both password match
    useEffect((e) => {

        if(userEmail !== '' && userPassword1 !== '' && userPassword2 !== '' 
            && (userPassword1 === userPassword2)) {
            setBtnIsActive(true);
        } else {
            setBtnIsActive(false);
        }
    }, [userEmail, userPassword1, userPassword2 ])



    //      [FUNCTION TO REGISTER ON SUBMIT]
    //      [CHECK IF THE EMAIL ALREADY EXIST]
    //      [IF NOT PROCEED TO CREATE, IF YES THROW ERROr]

    function registerUser(e) {
        //preven the page to refresh after clicking submit
        e.preventDefault();

        //localhost:4000/users/register
        // http://localhost:4000/orders/createOrder

        //  [CHECK IF THE EMAIL ALREADY EXIST]
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers : {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                user_email : userEmail
            })  
        })
        .then(res => res.json())
        .then(data => {
            
            //testing only
            console.log('data: ' + data);

            //test the returned data
            //if the data is false, fire the sweetAlert
            if(data === false) {
                
                Swal.fire({
                    title : 'Duplicate email found!',
                    icon: "error",
                    text : "Failed to register!"
                })

            //after checking if email exists 
            //send another request to finally register
            } else {

                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method : 'POST',
                    headers : {
                        'Content-Type' : 'application/json'
                    },
                    body : JSON.stringify({
                        user_email : userEmail,
                        user_password : userPassword1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    //test the returned data
                    //if true = success
                    //if false = failed
                    if (data === true) {

                        Swal.fire({
                            title : 'Successful!',
                            icon : "success",
                            text: `${userEmail} is now registered user`
                        })

                        //after successful register
                        //clear the fields
                        //direct the login
                        setUserEmail('');
                        setUserPassword1('');
                        setUserPassword2('');
                        navigate("/login");


                    } else {

                        Swal.fire({
                            title: 'Something is wrong.',
                            icon : "error",
                            text: "Failed to register."
                        })
                    }
                })
            }

        })

    }



    return (

        
        <Form onSubmit={(e) => registerUser(e)}>

        {/* TITLE PAGE */}
        <Form.Group className="mb-4 mt-3">
            <h4>Register</h4>
        </Form.Group>


        {/* EMAIL FIELD */}
        <Form.Group className="mb-4 mt-3" controlId="formRegUserEmail">
          <Form.Label>Email address</Form.Label>

          <Form.Control type="email" placeholder="Enter email"
          value={userEmail} onChange={e => setUserEmail(e.target.value)} required
          />
        </Form.Group>

        {/* PASSWORD1 */}
        <Form.Group className="mb-4" controlId="formRegUserPass1">
          <Form.Label>Password</Form.Label>

          <Form.Control type="password" placeholder="Password" 
            value={userPassword1} onChange={e => setUserPassword1(e.target.value)} required
          />
        </Form.Group>
        
        {/* PASSWORD2 */}
        <Form.Group className="mb-4" controlId="formRegUserPass2">
          <Form.Label>Confirm Password</Form.Label>

          <Form.Control type="password" placeholder="Password" 
            value={userPassword2} onChange={e => setUserPassword2(e.target.value)} required
          />
        </Form.Group>

    

        {/* NOTE ONLY */}
        <Form.Group className='mb-3'>
            <Form.Text className="text-muted">
                We'll never share your email with anyone else.
            </Form.Text>
        </Form.Group>



        {
            (btnIsActive) ?
            <Button variant="success" type="submit">
                Register
            </Button>
            :
            <Button variant="success" type="submit" disabled>
                Register
            </Button>
        }

        {/* BUTTON */}



      </Form>



    ) //end of return


} //end of Register()