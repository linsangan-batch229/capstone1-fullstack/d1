import { Card, Button, Col } from "react-bootstrap"
import {Link} from 'react-router-dom';


export default function Product() {

    return(
        <>

<Col className="d-flex justify-content-evenly mt-5">

    {/* NUMBER 1 */}
    <Card style={{ width: '18rem' }} className="d-flex justify-content-evenly">
      <Card.Img variant="top" src="https://images5.alphacoders.com/558/558718.jpg" />
      <Card.Body>
        <Card.Title>Hellblade Senua's Sacrifice</Card.Title>
        <Card.Text variant="success">
            Set in a dark fantasy world inspired by Norse mythology and Celtic culture
        </Card.Text>

        <Card.Subtitle style={{textAlign: "right"}}>Php 2,500</Card.Subtitle>
        <Card.Text> </Card.Text>
        <Card.Subtitle style={{textAlign: "right"}}>15pcs left</Card.Subtitle>
        
      </Card.Body>
      <Button variant=  "warning" as={Link} to="/createOrder">Order now</Button>
    </Card>
    
    {/* NUMBER 2 */}
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://th.bing.com/th/id/R.ee70c9a1ff5b29a80a667ece2d9be5bb?rik=%2b6DxXA4sEp1qEQ&riu=http%3a%2f%2fwww.wallpaperup.com%2fuploads%2fwallpapers%2f2014%2f01%2f29%2f241684%2f7e774f35497da8bc605eb93fd5789a21.jpg&ehk=ezLMOmSkFvtKpt%2fgGEcfk9DIa2RZg44DWiRyC%2fdvz4w%3d&risl=&pid=ImgRaw&r=0" />
      <Card.Body>
        <Card.Title>The Elder Scrolls V: Skyrim</Card.Title>
        <Card.Text>
        The lore is set in the Skyrim province, which is located north of Cyrodiil.    
        </Card.Text>

        <Card.Subtitle style={{textAlign: "right"}}>Php 1,800</Card.Subtitle>
        <Card.Text> </Card.Text>
        <Card.Subtitle style={{textAlign: "right"}}>30pcs left</Card.Subtitle>


      </Card.Body>
      <Button variant="warning" as={Link} to="/createOrder">Order now</Button>
    </Card>

    {/* NUMBER 3 */}
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src="https://wallpaperaccess.com/full/2861575.jpg" />
      <Card.Body>
        <Card.Title>Doom 3</Card.Title>
        <Card.Text>
        Set on Mars in 2145, where a military has set up a scientific research facility.
        </Card.Text>

        <Card.Subtitle style={{textAlign: "right"}}>Php 2,300</Card.Subtitle>
        <Card.Text> </Card.Text>
        <Card.Subtitle style={{textAlign: "right"}}>25pcs left</Card.Subtitle>
    
      </Card.Body>
      <Button variant="warning" as={Link} to="/createOrder">Order now</Button>
    </Card>
</Col>


<Col className="d-flex justify-content-evenly mt-5">

{/* NUMBER 1 */}
<Card style={{ width: '18rem' }} className="d-flex justify-content-evenly">
  <Card.Img variant="top" src="https://images6.alphacoders.com/543/543424.jpg" />
  <Card.Body>
    <Card.Title>Wolfenstein</Card.Title>
    <Card.Text>
        An American Army captain  and his efforts to stop the Nazis from ruling over the world.
    </Card.Text>

        <Card.Subtitle style={{textAlign: "right"}}>Php 2,700</Card.Subtitle>
        <Card.Text> </Card.Text>
        <Card.Subtitle style={{textAlign: "right"}}>50pcs left</Card.Subtitle>

  </Card.Body>
    <Button variant="warning" as={Link} to="/createOrder">Order now</Button>
</Card>

{/* NUMBER 2 */}
<Card style={{ width: '18rem' }}>
  <Card.Img variant="top" src="https://wallpaperaccess.com/full/4022123.jpg" />
  <Card.Body>
    <Card.Title>Metro Exodus</Card.Title>
    <Card.Text>
    Set in the post-apocalyptic wasteland of the former Russian Federation and Kazakhstan.
    </Card.Text>

    <Card.Subtitle style={{textAlign: "right"}}>Php 3,200</Card.Subtitle>
    <Card.Text> </Card.Text>
    <Card.Subtitle style={{textAlign: "right"}}>40pcs left</Card.Subtitle>

  </Card.Body>
  <Button variant="warning" as={Link} to="/createOrder">Order now</Button>
</Card>

{/* NUMBER 3 */}
<Card style={{ width: '18rem' }}>
  <Card.Img variant="top" src="https://th.bing.com/th/id/OIP.zDNVsHqqDtxfidzi9Qq9fgHaEK?pid=ImgDet&rs=1" />
  <Card.Body>
    <Card.Title>Black Desert Online</Card.Title>
    <Card.Text>
    Takes place in a high fantasy setting and revolves around the conflict between two rival nations
    </Card.Text>

    <Card.Subtitle style={{textAlign: "right"}}>Php 1,600</Card.Subtitle>
    <Card.Text> </Card.Text>
    <Card.Subtitle style={{textAlign: "right"}}>40pcs left</Card.Subtitle>
   
  </Card.Body>
  <Button variant="warning" as={Link} to="/createOrder">Order now</Button>
</Card>
</Col>

       

    </>
    )

}