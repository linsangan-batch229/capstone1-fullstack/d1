import { Container } from "react-bootstrap"
import { Form, Button } from "react-bootstrap"
import { useState, useEffect } from "react"
import Swal from "sweetalert2";



export default function RetrieveAllProduct() {

    const [infoToSearch, setInfoToSearch] = useState('');
    const [retrieveBy, setRetrieveBy] = ('');

    
    //
    const [searchBtnIsActive, setSearchBtnIsActive] = useState();


    //          [DISABLE/ENABLE BUTTON]
    useEffect((e) => {

        if(infoToSearch !== '') {
            setSearchBtnIsActive(true);

        } else {
            setSearchBtnIsActive(false);
        }
    },[infoToSearch])


    //              [FUNCTION TO GET SPECIFIC PRODUCT]
    //              [USERS]
    function searchInfoOne(e) {
        e.preventDefault();
        //63d0711f81732b402e0375c6
        //63d07bbe5b41132d816c7437

        

        fetch(`${process.env.REACT_APP_API_URL}/products/getProduct/${infoToSearch}`, {
            headers: {
                'Content-Type' : 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {


            //throw error message 
            if (data === false) {

                //error message
                Swal.fire({
                    title : 'Details not found',
                    icon : 'error',
                    text : 'Double check your inputs'
                })
                
            } else {

                //process the received data
                //need to insert it to the array in order to process the data
                //since renderData works perfectly on array objects
                renderData([data]);

                Swal.fire({
                    title : 'Success',
                    icon : 'success',
                    text : ''
                })
             
                
            }
        })
    }


    //              [FUNCTION TO GET ACTIVE PRODUCTS]
    //              [USERS]


    function searchInfoActive(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/getProductActive`, {
            headers : {
                'Content-Type' : 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {

            //test the data from server
            console.log(data)
            renderData(data);

            Swal.fire({
                title : 'Success',
                icon : 'success',
                text : ''
            })
        })
    }



  
    //          [FUNCTION TO GET ALL PRODUCT]
    //          [ADMIN ONLY]

    function searchInfoAll(e){

        e.preventDefault();
        const token = localStorage.getItem('token');

        fetch(`${process.env.REACT_APP_API_URL}/products/getProductAll`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {

            //for verification
            console.log(data);

            //if the data === false, meaning not authorized
            if(data === false) {

                Swal.fire({
                    title : 'Not Authorized',
                    icon: 'error',
                    text : ''
                })

            //data === true
            //data === object
            } else {
                // CALL FUNCTION BASED ON SEARCH All/specific
                console.log("call render");
                renderData(data);

                Swal.fire({
                    title : 'Success',
                    icon: 'success',
                    text : ''
                })
            }
        })
    }




    //          [FUNCTION TO RENDER RECIEVED DATA FROM SERVER]
    function renderData(data) {

        document.querySelector('#renderResult').value  = '';
        // Show the number of items retrieved
        document.querySelector('#renderResult').value += `Retrieved Items : ${data.length}\n`

        for (let i = 0; i < data.length; i++) {

            let a = `ID : ${data[i]._id}`
            let b = `Name : ${data[i].product_name}`
            let c = `Description : ${data[i].product_description}`
            let d = `Price : ${data[i].product_price} `
            let e = `Quantity : ${data[i].product_quantity}`
            let f = `Active : ${data[i].product_isActive}`
            let g = `Created : ${data[i].product_createdOn}`

            document.querySelector('#renderResult').value += `\n${a}`;
            document.querySelector('#renderResult').value += `\n${b}`;
            document.querySelector('#renderResult').value += `\n${c}`;
            document.querySelector('#renderResult').value += `\n${d}`;
            document.querySelector('#renderResult').value += `\n${e}`;
            document.querySelector('#renderResult').value += `\n${f}`;
            document.querySelector('#renderResult').value += `\n${g}\n\n`;
        }
    }


    

    //          [FUNCTION SELECTOR OF 'searchInfoAll()', 'searchInfoActive()', 'searchInfoAll()' ]
    function searchBy(e) {

        //search all products
        if (infoToSearch === 'all' || infoToSearch === 'All') {
            searchInfoAll(e);
        
        //search all active products
        } else if (infoToSearch === 'active' || infoToSearch === 'Active') {
            searchInfoActive(e);

        } else {
            searchInfoOne(e);
        }
    }



    return (
    <>
        <Container  className='pt-4 pb-3 text-center' bg="warning">
            <h4>RETRIEVE PRODUCTS</h4>
        </Container>    


        <Container>
        <Form onSubmit={(e) => searchBy(e) }>
            <Form.Group className="mb-5" controlId="exampleForm.ControlInput1">
                <Form.Label>Search using Product ID / Active / All</Form.Label>
                <Form.Control 
                    type="string" 
                    placeholder="Search by" 
                    value={infoToSearch}
                    onChange={(e) => setInfoToSearch(e.target.value)}
                    required
                />

            </Form.Group>


            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                <Form.Label>Result</Form.Label>
                <Form.Control 
                    as="textarea" 
                    rows={10}  
                    value= ''
                    id = "renderResult"
                    disabled
                    
                />
            </Form.Group>

            {
                (searchBtnIsActive) ? 
                <Button variant="primary" type="submit">
                    Search
                </Button>
                :
                <Button variant="primary" type="submit" disabled>
                    Search
                </Button>
            }



        </Form>
        </Container>


    </>

    )


}