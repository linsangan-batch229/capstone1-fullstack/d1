import {Form, Button} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';

export default function Login(){

    const navigate = useNavigate();

    const {userAdmin, setUserAdmin} = useContext(UserContext);

    //state hooks
    const [loginEmail, setLoginEmail] = useState('');
    const [loginPassword, setLoginPassword] = useState('');
    const [loginBtnActive, setLoginBtnActive] = useState(false);



    //enable / disabled login button
    useEffect((e) => {
        if(loginEmail !== '' && loginPassword !== '') {
            setLoginBtnActive(true);
        } else {
            setLoginBtnActive(false);
        }

    }, [loginEmail, loginPassword])


    //          [FUNCTION]

     async function  authenticateUser(e) {

        // Prevents page redirection via form submission
	    e.preventDefault();

        //      [CHECK IF REGISTERED OR NOT]
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers : {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                user_email : loginEmail
            })
        })
        .then(res => res.json())
        .then(data => {

            //test the data

            //if the data is false, meaning not registered
            if (data === false) {

                Swal.fire({
                   title: "Failed login!",
                   icon : "error",
                   text: "You're not a registered user!"
                })

            //if the data returned is not false
            //send again a request, this time with password
            } else {

                fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
                    method : 'POST',
                    headers : {
                        'Content-Type' : 'application/json'
                    },
                    body : JSON.stringify({
                        user_email: loginEmail,
                        user_password: loginPassword
                    })
                })
                .then(res => res.json())
                .then( data => {            //async

                    //test the data
                    //if true , token
                    // if false error password
                    if(data === false) {

                        Swal.fire({
                            title : 'Incorrect password',
                            icon : "error",
                            text : ""
                        })
                    //return data is now token from this point
                    } else {
                        findIsAdmin(data.Token)
                        //local storage
                        localStorage.setItem('token', data.Token);
                        findIsAdmin(data.Token); 

                        
                        Swal.fire({
                            title : 'Success',
                            icon : "success",
                            text : ""
                        })

                        setLoginEmail('');
                        setLoginPassword('');
                        setLoginBtnActive(false);

                        navigate('/');
                       

                        
                       
                        
            
                    }

                })

            }

        })




    }


    //send request using token and find out
    //if the user is adnmin or not
    function  findIsAdmin(token) {

        fetch('http://localhost:4000/users/getDetailsFromToken', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {

            localStorage.setItem('id', data._id);
            localStorage.setItem('isAdmin', data.user_isAdmin);

            console.log("data: " + data.user_email);
            console.log("userAdmin: " + data.user_isAdmin);

        })
    }




    return(
    //render the register   
    <Form onSubmit={(e) => authenticateUser(e)}>

        <Form.Group className="mb-4 mt-3">
            <h4>Login</h4>
        </Form.Group>

        {/* EMAIL FIELD */}
        <Form.Group className="mb-4 mt-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
                type="email"
                placeholder="Enter email" 
                value={loginEmail}
                onChange={(e) => setLoginEmail(e.target.value)}
                required
            />
        </Form.Group>
    
        <Form.Group className="mb-4" controlId="formBasicPassword1">
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Password" 
                value={loginPassword}
                onChange={(e) => setLoginPassword(e.target.value)}
                required
                />
        </Form.Group>

        {/* enable / disabled register button */}
        {
        (loginBtnActive) ?
            <Button variant="primary" type="submit">
                Login
            </Button>
        :
            <Button variant="primary" type="submit" disabled>
                Login
            </Button>
        }


    </Form>

    )

}